var gulp = require("gulp"),
    data = require("gulp-data"),
    reactJadeTransformer = require("react-jade-transformer"),
    through = require("through2"),
    jade = require("jade"),
    mkdirp = require("mkdirp"),
    gutil = require("gulp-util"),
    React = require("react/addons"),
    componentLibrary = require("gulp-react-component-library"),
    es = require("event-stream"),
    fs = require("fs"),
    streamFromArray = require("stream-from-array"),
    envify = require("envify/custom"),
    watchify = require("watchify"),
    source = require("vinyl-source-stream"),
    browserify = require("browserify"),
    using = require("gulp-using"),
    connect = require("gulp-connect"),
    babel = require("gulp-babel");

var PACKED = false;
var WATCH = false;
var DEST_COMPILE = ".tmp/js";
var JADE_WRAPPER = "./front-end/componentLibraryWrapper.jade";
var DEST_DIR = ".tmp/www";


var dispatchErrors = function(pluginName, closeTask) {
  return function(err) {
    if (closeTask) {
      this.emit("end");
    }

    console.log("ERROR - " + pluginName);
    gutil.log(err.message);
    console.log("---------------------");
  }
}


gulp.task('browserify', ["js"], function() {
  var bundler = browserify({
    cache: {}, // watchify arguments
    packageCache: {}, // watchify arguments
    fullPaths: true, // watchify arguments
    entries: [DEST_COMPILE + '/front-end/app.js'],
    debug: true
  }).transform(envify({
    ISO_ENV: "client",
    MEDIA_URL_BASE: "/media/cache",
    NODE_ENV: PACKED ? "production" : "development"
  }))

  var bundle = function() {
    var stream = bundler
      .exclude("react/addons")
      .bundle()
      .on("error", dispatchErrors("browserify", true))
      .pipe(source("app-bundle.js"))
      .pipe(gulp.dest(DEST_DIR + '/assets/js'));

    if (PACKED) {
      stream = stream
        .pipe(streamify(uglify()))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST_DIR + '/assets/js'));
    }

    return stream
  };

  if (WATCH) {
    bundler = watchify(bundler);
    bundler = bundler.on('update', bundle);
    bundler = bundler.on("time", function(time) {
      console.log("Javascript Bundle updated: " + time + "ms");
    });
    bundler = bundler.on("error", dispatchErrors("browserify", false));
  }

  return bundle();
});


gulp.task('js', function() {
  var stream = gulp.src([
    "./front-end/**/*.js",
    "./front-end/*.js",
  ], {
    base: "./"
  });

  stream = stream.pipe(reactJadeTransformer.gulp({prepare: true}))
  stream = stream.pipe(babel())

  if (WATCH) {
    steam = stream.on('error', dispatchErrors("babel", false))
  }

  return stream.pipe(reactJadeTransformer.gulp({transform: true}))
               .pipe(gulp.dest(DEST_COMPILE))
               .pipe(using());
});


gulp.task('flushRequires', function() {
  Object.keys(require.cache).filter(function (cachePath) {
    return (
      cachePath.indexOf(DEST_COMPILE+"/front-end/") != -1 ||
      cachePath.indexOf("/data/") != -1
    );
  }).forEach(function (cachePath) {
    require.cache[cachePath] = null;
  });
});


gulp.task("templates", ["flushRequires"], function() {
  var stream = gulp.src([
    DEST_COMPILE+"/front-end/**/*.react.js",
  ], { base: DEST_COMPILE+"/front-end" });

  return stream
    .pipe(componentLibrary({
      data: "./data",
      dest: DEST_DIR,
      wrapper: JADE_WRAPPER,
    }))
    .pipe(connect.reload())
    .pipe(using());
});


gulp.task("dependencies", function() {
  gulp.src([
    "./node_modules/react/dist/react-with-addons.js",
  ]).pipe(gulp.dest(DEST_DIR + "/assets/js"))
    .pipe(using());
});




gulp.task("watch", function() {
  gulp.watch("./front-end/**/*.js", ["js"]);
  gulp.watch("./.tmp/js/front-end/**/*.react.js", ["templates"]);
  gulp.watch("./data/**/*.js", ["templates"]);
});


gulp.task("default", [ "templates", "dependencies" ], function() {
  WATCH = true;
  gulp.start("browserify");
  gulp.start("watch");
  connect.server({
    port: 8888,
    root: ".tmp/www",
    livereload: true
  });
});
