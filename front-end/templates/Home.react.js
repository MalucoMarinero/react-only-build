var React = typeof window !== 'undefined' ? window.React : require("react/addons");
var TestComponent = require("../ui/TestComponent.react.js");

var DOM = React.DOM;

module.exports = React.createClass({
  statics: {
    getPageTitle: function(props) {
      return "Home Page";
    },
  },

  getInitialState: function() {
    return {count: 0};
  },

  componentDidMount: function() {
    setInterval(() => {
      this.setState({count: this.state.count + 1});
    }, 1000);
  },

  onButtonClick: function() {
    this.setState({count: this.state.count + 100});
  },

  render: function() {
    return rj`
      .pageWrapper.pager
        h1.heading= this.props.title
        p= "Loaded for " + this.state.count + " seconds"
        p Duran, Duran.
        p Something, something.
        +TestComponent(
          onClick=this.onButtonClick
          style={paddingLeft: 500}
        )
        h2 Hello
        p Some text in heyah.
        ul
          li Green
          li Blue
          li Yellow
          li Red
    `;
  },
});
