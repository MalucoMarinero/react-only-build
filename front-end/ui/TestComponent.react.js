var React = typeof window !== 'undefined' ? window.React : require("react/addons");

var DOM = React.DOM;
var _ = require("lodash");

module.exports = React.createClass({
  statics: {
    applyToStyleguide: true,
  },

  getInitialState: function() {
    return {count: this.props.initialCount || 0, haveClass: true};
  },

  incrementClick: function(e) {
    if (this.props.onClick) {
      this.props.onClick(e);
    }
    this.setState({
      count: this.state.count * 3 + 1,
      haveClass: !this.state.haveClass
    });
  },

  render: function() {
    var theClass = this.state.haveClass ? "something" : null;
    return rj`
      button(
        class=theClass
        onClick=this.incrementClick
      )= "Count is at " + this.state.count
    `;
  },
});
