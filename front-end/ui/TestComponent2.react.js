var React = typeof window !== 'undefined' ? window.React : require("react/addons");

var DOM = React.DOM;

module.exports = React.createClass({
  statics: {
    applyToStyleguide: true
  },

  render: function() {
    return rj`
      h2.heading!= "<em>Hello</em>"
    `;
  },
});
