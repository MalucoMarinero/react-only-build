module.exports.default = {};

module.exports.withInitial = {initialCount: 50};

module.exports.withHigh = {initialCount: 10000};

module.exports.withExtraHigh = {initialCount: 200000};

module.exports.withExtremelyHigh = {initialCount: 60000000};
